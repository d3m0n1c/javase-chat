rd /q/s bin
mkdir bin
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/server/controller/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/server/model/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/server/model/interfaces/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/server/view/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/server/view/interfaces/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/common/*.java
jar cvfm server.jar res/server/manifest.mf -C bin .
rd /q/s bin
pause