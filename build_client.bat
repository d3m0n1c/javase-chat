rd /q/s bin
mkdir bin
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/client/controller/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/client/model/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/client/model/interfaces/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/client/view/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/client/view/interfaces/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/client/common/*.java
javac -source 1.5 -target 1.5 -d bin -sourcepath src src/ua/vydai/chat/common/*.java
jar cvfm client.jar res/client/manifest.mf -C bin .
rd /q/s bin
pause