Simple Java SE chat that was coded at programming courses in 2011 as a laboratory task.

To build server or client JARs run **build_server.bat** or **build_client.bat** appropriately.

To run server or client run **server.bat** or **client.bat** appropriately.