package ua.vydai.chat.client.common;

public interface ModelToViewCommands {
    
    String VIEW_INFO_MSG = "IMSG ";
    String VIEW_ERROR_MSG = "EMSG ";
    String VIEW_NAMED_MSG = "NMSG ";
    String VIEW_USERLIST = "VULI ";
    String VIEW_ADD_USER = "VAUS ";
    String VIEW_REM_USER = "VRUS ";
    String VIEW_CONNECT_STARTED = "VCSD";
}
