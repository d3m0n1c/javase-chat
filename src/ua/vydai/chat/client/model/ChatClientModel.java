package ua.vydai.chat.client.model;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;

import ua.vydai.chat.client.common.ModelToViewCommands;
import ua.vydai.chat.client.model.interfaces.ClientModel;
import ua.vydai.chat.common.ClientServerCommands;

public class ChatClientModel extends Observable implements Runnable, ClientModel {

    public ChatClientModel() { }
    
    public void run() {
        Thread currentThread = Thread.currentThread();
        while(clientThread == currentThread) {
            try {
                String msg = input.readLine();
                if (msg == null) { // if input stream is empty
                    break; // end message checking
                }
                // check message by protocol
                if (msg.startsWith(ClientServerCommands.USERLIST)) {
                    setClientStatus(true); // successfully connected
                    notifyView(ModelToViewCommands.VIEW_INFO_MSG + "Successfully connected to the server.");
                    notifyView(ModelToViewCommands.VIEW_USERLIST + msg.substring(ClientServerCommands.USERLIST.length()));
                } else if (msg.startsWith(ClientServerCommands.GENERAL_MESSAGE)) {
                    notifyView(ModelToViewCommands.VIEW_NAMED_MSG + msg.substring(ClientServerCommands.GENERAL_MESSAGE.length()));
                } else if (msg.startsWith(ClientServerCommands.ADD_USER)) {
                    notifyView(ModelToViewCommands.VIEW_ADD_USER + msg.substring(ClientServerCommands.ADD_USER.length()));
                } else if (msg.startsWith(ClientServerCommands.REMOVE_USER)) {
                    notifyView(ModelToViewCommands.VIEW_REM_USER + msg.substring(ClientServerCommands.REMOVE_USER.length()));
                } else if (msg.startsWith(ClientServerCommands.USER_EXISTS)) {
                    notifyView(ModelToViewCommands.VIEW_ERROR_MSG + "User with this name already exist.");
                    quitConnection();
                } else if (msg.startsWith(ClientServerCommands.SOCKET_EXISTS)) {
                    notifyView(ModelToViewCommands.VIEW_ERROR_MSG + "You are already connected.");
                    quitConnection();
                } else if (msg.startsWith(ClientServerCommands.STOP_SERVER)) {
                    notifyView(ModelToViewCommands.VIEW_INFO_MSG + "SERVER SHUTDOWN");
                    quitConnection();
                }
                
                // add new type of messages from server below
                
            } catch(IOException e) {
                quitConnection();
            }
        }
    }
    
    public void connect(String username, String host, int port) {
        if (isConnected()) {
            return;
        }
        this.username = username;
        notifyView(ModelToViewCommands.VIEW_CONNECT_STARTED);
        
        // Create client socket
        try {
            socket = new Socket(host, port);
        } catch (UnknownHostException e) {
            notifyView(ModelToViewCommands.VIEW_ERROR_MSG + "Can't determine address: "+host+":"+port);
            return;
        } catch (IOException e) {
            notifyView(ModelToViewCommands.VIEW_ERROR_MSG + "Can't connect to address: "+host+":"+port);
            return;
        }
        
        // Start I/O Streams
        try {
            output = new BufferedWriter(
                    new OutputStreamWriter(
                        socket.getOutputStream(), "UTF8"));
            input  = new BufferedReader(
                    new InputStreamReader(
                        socket.getInputStream(), "UTF8"));
        } catch (IOException e) {
            notifyView(ModelToViewCommands.VIEW_ERROR_MSG + "Error with I/O stream start");
            quitConnection();
            return;
        }
        
        // Start client
        setClientStatus(false); // Reset connection flag until server don't get response in Thread
        clientThread = new Thread(this);
        // Activate message listener. Ready to get response from server.
        clientThread.start(); 
        // Say "Hello" to server and catch response in started thread
        sendMessageToServer(ClientServerCommands.HELLO + username);
    }

    public void submitMessage(String msg) {
        if (!isConnected()) {
            return;
        }
        sendMessageToServer(ClientServerCommands.USER_MESSAGE + msg);
//        notifyView(ModelToViewCommands.VIEW_NAMED_MSG + username + ": " + msg);
    }

    public void disconnect() {
        if (!isConnected()) {
            return;
        }
        sendMessageToServer(ClientServerCommands.QUIT + username);
        notifyView(ModelToViewCommands.VIEW_INFO_MSG + "DISCONNECTED");
        quitConnection();
    }

    public Observable observable() {
        return this;
    }
    
    public boolean isConnected() {
        return connected;
    }
    
    private void setClientStatus(boolean isConnected) {
        connected = isConnected;
        notifyView(null);
    }
    
    private void notifyView(Object obj) {
        setChanged();
        notifyObservers(obj);
    }
    
    private void sendMessageToServer(String msg) {
        try {
            output.write(msg+"\r\n");
            output.flush();
        } catch(IOException e) {
            notifyView(ModelToViewCommands.VIEW_ERROR_MSG + "Message send failed");
            quitConnection();
        }
    }
    
    private void quitConnection() {
        if (clientThread != null) {
            clientThread = null;
        } 
        if (socket != null) {
            try {
                socket.close(); 
            } catch(IOException e) {
                System.out.println("Can't close client Socket");
            } finally {
                socket = null;
            }
        }
        setClientStatus(false);
    }
    
    private String username;
    private BufferedReader input;
    private BufferedWriter output;
    private Thread clientThread;
    private Socket socket;
    private boolean connected = false;
}
