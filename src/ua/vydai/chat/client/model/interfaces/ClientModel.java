package ua.vydai.chat.client.model.interfaces;

import java.util.Observable;

public interface ClientModel {
    
    void connect(String username, String host, int port);
    void submitMessage(String msg);
    void disconnect();
    
    boolean isConnected();
    
    Observable observable();
}
