package ua.vydai.chat.client.view.interfaces;

import ua.vydai.chat.client.model.interfaces.ClientModel;
import ua.vydai.chat.client.view.interfaces.ClientView;

public interface ClientViewFactory {
    
    ClientView createView(ClientModel model);
}
