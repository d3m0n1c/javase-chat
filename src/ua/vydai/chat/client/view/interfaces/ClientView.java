package ua.vydai.chat.client.view.interfaces;

import java.awt.event.ActionListener;

public interface ClientView {
    
    // View actions
    String ACTION_MESSAGE = "message";
    String ACTION_CONNECT = "connect";
    String ACTION_DISCONNECT = "disconnect";
    String ACTION_CLOSE = "close";
    
    // Methods to listener control
    void addActionListener(ActionListener al);
    void removeActionListener(ActionListener al);
    
    // Methods to get/set information
    String getUsername();
    String getHost();
    String getPort();
    String getMessage();
    
    // View control
    void show();
    void close();
    void showError(String msg);
}
