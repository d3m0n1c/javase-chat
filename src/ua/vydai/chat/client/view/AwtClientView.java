package ua.vydai.chat.client.view;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import ua.vydai.chat.client.common.ModelToViewCommands;
import ua.vydai.chat.client.model.interfaces.ClientModel;
import ua.vydai.chat.client.view.interfaces.ClientView;

public abstract class AwtClientView implements ClientView, Observer {
    
    public AwtClientView(ClientModel model) {
        this.model = model;
        model.observable().addObserver(this);
        
        createFrame();
        update(null, null);
    }
    
    protected abstract void createFrame();
    
    protected abstract void clearInputFrame();
    
    public void addActionListener(ActionListener listener) {
        listeners.add(listener);
    }
    
    public void removeActionListener(ActionListener listener) {
        listeners.remove(listener);
    }
    
    public void show() {
        frame.setVisible(true);
    }
    
    public void close() {
        frame.setVisible(false);
        frame.dispose();
    }
    
    public void showError(String msg) {
        printMessage("ERROR: "+msg);
    }
    
    public void update(Observable o, Object arg) {
        if (arg instanceof String) {
            String cmd = (String) arg;
            if (cmd.startsWith(ModelToViewCommands.VIEW_NAMED_MSG)) {
                printMessage(cmd.substring(ModelToViewCommands.VIEW_NAMED_MSG.length()));
            } else if (cmd.startsWith(ModelToViewCommands.VIEW_INFO_MSG)) {
                printMessage(cmd.substring(ModelToViewCommands.VIEW_INFO_MSG.length()));
            } else if (cmd.startsWith(ModelToViewCommands.VIEW_ERROR_MSG)) {
                showError(cmd.substring(ModelToViewCommands.VIEW_ERROR_MSG.length()));
            } else if (cmd.startsWith(ModelToViewCommands.VIEW_ADD_USER)) {
                String userName = cmd.substring(ModelToViewCommands.VIEW_ADD_USER.length());
                addUser(userName);
                printMessage(">>> "+userName+" has joined to chat.");
            } else if (cmd.startsWith(ModelToViewCommands.VIEW_REM_USER)) {
                String userName = cmd.substring(ModelToViewCommands.VIEW_REM_USER.length());
                remUser(userName);
                printMessage("<<< "+userName+" has left from chat.");
            } else if (cmd.startsWith(ModelToViewCommands.VIEW_USERLIST)) {
                String[] users = cmd.substring(ModelToViewCommands.VIEW_USERLIST.length()).split(";");
                for (String userName : users) {
                    addUser(userName);
                }
            } else if (cmd.startsWith(ModelToViewCommands.VIEW_CONNECT_STARTED)) {
                clearInputFrame();
                printMessage("CONNECT TO THE SERVER");
            }
        }
    }
    
    protected void fireAction(String command) {
        ActionEvent event = new ActionEvent(this, 0, command);
        for (ActionListener al : listeners) {
            al.actionPerformed(event);
        }
    }
    
    protected abstract void printMessage(String msg);
    
    protected abstract void addUser(String userName);
    
    protected abstract void remUser(String userName);
    
//    protected abstract void unblockOutput();
    
    protected Frame frame;
    protected ClientModel model;
    
    private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
}
