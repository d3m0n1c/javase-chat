package ua.vydai.chat.client.view;


import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Random;

import ua.vydai.chat.client.model.interfaces.ClientModel;
import ua.vydai.chat.client.view.interfaces.ClientView;
import ua.vydai.chat.client.view.interfaces.ClientViewFactory;

public class SimpleAwtClientView extends AwtClientView {
    
    private static final String FRAME_NAME = "Chat Client";
    private static final String MENU1 = "Chat";
    private static final String MENU_CONNECT = "Connect";
    private static final String MENU_DISCONNECT = "Disconnect";
    
    public SimpleAwtClientView(ClientModel model) {
        super(model);
    }
    
    public static ClientViewFactory getFactory() {
        return new ClientViewFactory() {
            public ClientView createView(ClientModel model) {
                return new SimpleAwtClientView(model);
            }
        };
    }
    
    @Override
    protected void createFrame() {
        frame = new Frame(FRAME_NAME);
        
        /*Window Closing Event Section*/
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                loginDialog.dispose();
                fireAction(ACTION_CLOSE);
            } 
        });
        
        /*Frame settings*/
        frame.setSize(500, 400);
        frame.setResizable(false);       
        frame.setLayout(new BorderLayout());
        
        /*Menu creation*/
        MenuBar menuBar = new MenuBar();
        Menu menu = new Menu(MENU1);
        // Connect
        menuConnect = new MenuItem(MENU_CONNECT);
        menuConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loginDialog.setVisible(true);
            }
        });
        menu.add(menuConnect);
        // Disconnect
        menuDisconnect = new MenuItem(MENU_DISCONNECT);
        menuDisconnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireAction(ACTION_DISCONNECT);
            }
        });
        menu.add(menuDisconnect);
        
        menuBar.add(menu);
        frame.setMenuBar(menuBar);
        
        /* Create Panel for User List*/
        Panel userListPanel = new Panel(new BorderLayout());
        frame.add("East", userListPanel);
        
        /*UserList creation*/
        userList = new List();
        userList.setEnabled(false);
        userListPanel.add("Center", userList);
        
        /* Create Panel for In/Out Message boxes*/
        Panel msgsPanel = new Panel(new BorderLayout());
        frame.add("Center", msgsPanel);
        
        /*Input messages box creation*/
        inputMsgs = new TextArea("", 0, 0, TextArea.SCROLLBARS_VERTICAL_ONLY);
        inputMsgs.setEditable(false);
        msgsPanel.add("Center", inputMsgs);
        
        /*Output message field creation*/
        outputMsg = new TextField();
        outputMsg.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {}
            public void keyReleased(KeyEvent e) {}
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    fireAction(ACTION_MESSAGE);
                }
            }
        });
        msgsPanel.add("South", outputMsg);
        loginDialog = new LoginDialog(frame);
    }
    
    @Override
    protected void printMessage(String msg) {
        inputMsgs.append(msg+"\r\n");
    }
    
    @Override
    protected void addUser(String userName) {
        userList.add(userName);
    }
    
    @Override
    protected void remUser(String userName) {
        userList.remove(userName);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        super.update(o, arg); // MANDATORY LINE
        boolean isConnected = model.isConnected();
        if (!isConnected) {
            userList.removeAll();
        }
        userList.setBackground(isConnected?Color.WHITE:Color.GRAY);
        outputMsg.setEnabled(isConnected);
        outputMsg.setBackground(isConnected?Color.WHITE:Color.GRAY);
        menuConnect.setEnabled(!isConnected);
        menuDisconnect.setEnabled(isConnected);
    }
    
    public String getUsername() {
        return loginDialog.getUserName();
    }
    
    public String getHost() {
        return loginDialog.getHost();
    }
    
    public String getPort() {
        return loginDialog.getPort();
    }
    
    public String getMessage() {
        try {
            return outputMsg.getText();
        } finally {
            outputMsg.setText("");
        }
    }
    
    protected void clearInputFrame() {
        inputMsgs.setText("");
    }
    
    public void showLogin() {
        loginDialog.setVisible(true);
    }
    
    @SuppressWarnings("serial")
    private class LoginDialog extends Dialog {

        public LoginDialog(Window owner) {
            super(owner, ModalityType.APPLICATION_MODAL);
            createFrame();
        }
        
        private void createFrame() {
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    setVisible(false);
                } 
            });
            setSize(200, 150);
            setResizable(false);
            setBackground(Color.GRAY);        
            setLayout(new GridLayout(4, 1));
            
            Panel p1 = new Panel(new BorderLayout());
            Label labelUserName = new Label("Username:");
            p1.add("West", labelUserName);
            Random r= new Random();
            String rand = (""+(100000+r.nextInt(100000))).substring(1);
            userName = new TextField("user"+rand);
            p1.add("Center", userName);
            add(p1);
            
            Panel p2 = new Panel(new BorderLayout());
            Label labelHost = new Label("Host:");
            p2.add("West", labelHost);
            host = new TextField("localhost");
            p2.add("Center", host);
            add(p2);
            
            Panel p3 = new Panel(new BorderLayout());
            Label labelPort = new Label("Port:");
            p3.add("West", labelPort);
            port = new TextField("4444");
            p3.add("Center", port);
            add(p3);
            
            Button connect = new Button("Connect");
            connect.setBounds(66, 95, 68, 23);
            connect.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                  setVisible(false);
                  fireAction(ACTION_CONNECT);
                }
            });
            add(connect);
        }
        
        public String getUserName() {
            return userName.getText();
        }
        
        public String getHost() {
            return host.getText();
        }
        
        public String getPort() {
            return port.getText();
        }
        
        private TextField userName;
        private TextField host;
        private TextField port;
    }
    
    List userList;
    TextArea inputMsgs;
    TextField outputMsg;
    MenuItem menuConnect;
    MenuItem menuDisconnect;
    LoginDialog loginDialog;
}
