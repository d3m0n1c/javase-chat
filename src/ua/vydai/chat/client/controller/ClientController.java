package ua.vydai.chat.client.controller;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import ua.vydai.chat.client.model.ChatClientModel;
import ua.vydai.chat.client.model.interfaces.ClientModel;
import ua.vydai.chat.client.view.SimpleAwtClientView;
import ua.vydai.chat.client.view.interfaces.ClientView;
import ua.vydai.chat.client.view.interfaces.ClientViewFactory;

public class ClientController implements ActionListener {
    
    public static void main(String[] args) {
        ClientController controller = new ClientController(new ChatClientModel());
        controller.createView(SimpleAwtClientView.getFactory());
//        controller.createView(SimpleAwtClientView.getFactory()); // FOR DEBUG
    }
    
    public ClientController(ClientModel model) {
        this.model = model;
    }
    
    public void actionPerformed(ActionEvent event) {
        ClientView view = (ClientView) event.getSource();
        if (event.getActionCommand().equals(ClientView.ACTION_MESSAGE)) {
            String msg = view.getMessage();
            if (msg.trim().length() > 0) {
                model.submitMessage(msg);
            }
        } else if (event.getActionCommand().equals(ClientView.ACTION_CONNECT)) {
            String username = view.getUsername().trim();
            if (username.length() == 0) {
                view.showError("Username can't be empty");
                return;
            }
            String host = view.getHost().trim();
            int port;
            try {
                port = Integer.parseInt(view.getPort());
            } catch (NumberFormatException e) {
                view.showError("Port should contain only digits");
                return;
            }
            if (port < 1024 || port > 65536) {
                view.showError("Port must be between 1024 and 65536");
                return;
            }
            model.connect(username, host, port);
        } else if (event.getActionCommand().equals(ClientView.ACTION_DISCONNECT)) {
            model.disconnect();
        } else if (event.getActionCommand().equals(ClientView.ACTION_CLOSE)) {
            view.close();
            views.remove(view);
            if (views.size() == 0) {
                model.disconnect();
                System.exit(0);
            }
        }
    }
    
    public void createView(ClientViewFactory factory) {
        ClientView view = factory.createView(model);
        views.add(view);
        view.addActionListener(this);
        view.show();
    }
    
    private List<ClientView> views = new ArrayList<ClientView>();
    private ClientModel model;
}
