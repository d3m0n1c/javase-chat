package ua.vydai.chat.common;

public interface ClientServerCommands {
    
    // to Server
    String HELLO = "HELO "; // HELO userName
    String QUIT = "QUIT "; // QUIT (w/o args)
    String USER_MESSAGE = "UMSG "; // UMSG message
    
    // to Client
    String USERLIST = "LIST "; // LIST userList (names separated by ';')
    String ADD_USER = "UADD "; // UADD userName
    String REMOVE_USER = "UREM "; // UREM userName
    String GENERAL_MESSAGE = "GMSG "; // GMSG userName:message
    String USER_EXISTS = "EXIS"; // EXIS (w/o args)
    String SOCKET_EXISTS = "SOCK"; // SOCK (w/o args)
    String STOP_SERVER = "STOP"; // STOP (w/o args)
}
