package ua.vydai.chat.server.view;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


import ua.vydai.chat.server.model.interfaces.ServerModel;
import ua.vydai.chat.server.view.interfaces.ServerView;

public abstract class AwtServerView implements ServerView, Observer {

    public AwtServerView(ServerModel model) {
        this.model = model;
        model.observable().addObserver(this);
        
        createFrame();
        update(null, null);
    }
    
    protected abstract void createFrame();
    
    public abstract void update(Observable source, Object arg);
    
    public void show() {
        frame.setVisible(true);
    }
    
    public void close() {
        frame.setVisible(false);
        frame.dispose();
    }
    
    public void addActionListener(ActionListener listener) {
        listeners.add(listener);
    }

    public void removeActionListener(ActionListener listener) {
        listeners.remove(listener);
    }
    
    protected void fireAction(String command) {
        ActionEvent event = new ActionEvent(this, 0, command);
        for (ActionListener al : listeners) {
            al.actionPerformed(event);
        }
    }
    
    protected Frame frame;
    protected ServerModel model;
    
    private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
}
