package ua.vydai.chat.server.view.interfaces;

import ua.vydai.chat.server.model.interfaces.ServerModel;

public interface ServerViewFactory {
    
    ServerView createView(ServerModel model);
}
