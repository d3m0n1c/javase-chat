package ua.vydai.chat.server.view.interfaces;

import java.awt.event.*;

public interface ServerView {
    
    // Actions
    String ACTION_START = "start";
    String ACTION_STOP = "stop";
    String ACTION_CLOSE  = "close";
    
    // Methods to listener control
    void addActionListener(ActionListener al);
    void removeActionListener(ActionListener al);
    
    // Methods to get/set information
    String getPort();
    
    // Methods to view control
    void show();
    void close();  
    void showError(String msg);
}
