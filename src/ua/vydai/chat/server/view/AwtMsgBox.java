package ua.vydai.chat.server.view;

import java.awt.*; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class AwtMsgBox extends Dialog implements ActionListener
{  
    
    /**
     * @param frame parent frame 
     * @param msg message to be displayed
     */
    private AwtMsgBox(Frame frame, String msg) {
        super(frame, "Message", true);
        setLayout(new BorderLayout());
        
        add("Center", new Label(msg));
        
        Panel p = new Panel();
        p.setLayout(new FlowLayout());
        p.add(ok = new Button("OK"));
        ok.addActionListener(this);
        add("South", p);
        
        Dimension d = getToolkit().getScreenSize();
        setLocation(d.width/3, d.height/3);
        pack();
        setVisible(true);
    }
    
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == ok) {  
            setVisible(false);
        }
    }
    
    public static void showMessage(Frame frame, String msg) {
        AwtMsgBox messageBox = new AwtMsgBox(frame, msg);
        messageBox.dispose();
        return;
    }
    
    public static void main(String args[]) {
        Frame f = new Frame();
        AwtMsgBox.showMessage(f, "Test message");
        f.dispose();
    }
    
    private Button ok;
}