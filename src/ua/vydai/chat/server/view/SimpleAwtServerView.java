package ua.vydai.chat.server.view;

import java.awt.*;
import java.awt.event.*;
import java.util.Observable;

import ua.vydai.chat.server.model.interfaces.ServerModel;
import ua.vydai.chat.server.view.interfaces.ServerView;
import ua.vydai.chat.server.view.interfaces.ServerViewFactory;

public class SimpleAwtServerView extends AwtServerView {
    
    private static final String FRAME_NAME = "Chat Server";
    private static final String PORT_LABEL_TEXT = "Port: ";
    private static final String START_BUTTON_NAME = "START SERVER";
    private static final String STOP_BUTTON_NAME = "STOP SERVER";
    
    public SimpleAwtServerView(ServerModel model) {
        super(model);
    }
    
    public static ServerViewFactory getFactory() {
        return new ServerViewFactory() {
            public ServerView createView(ServerModel model) {
                return new SimpleAwtServerView(model);
            }
        };
    }
    
    protected void createFrame() {
        frame = new Frame(FRAME_NAME);
        
        /*Window Closing Event Section*/
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                fireAction(ACTION_CLOSE);
            } 
        });
        
        /*Frame settings*/
        frame.setSize(175,145);
        frame.setResizable(false);
        frame.setBackground(Color.black);        
        frame.setLayout(new BorderLayout());
        
        Panel centerPanel = new Panel(null);
        /*Label for port editbox*/
        lbPort = new Label(PORT_LABEL_TEXT);
        lbPort.setForeground(Color.white);
        lbPort.setBounds(30,10,30,20);
        centerPanel.add(lbPort);
        
        /*Edit box for port value*/
        edPort = new TextField("4444");
        edPort.setBounds(70,10,50,20);
        centerPanel.add(edPort);
        
        /*Start button*/
        cmdStart = new Button(START_BUTTON_NAME);
        cmdStart.setBounds(10,40,150,30);
        cmdStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireAction(ACTION_START);
            }
        });
        centerPanel.add(cmdStart);
        
        /*Stop button*/
        cmdStop = new Button(STOP_BUTTON_NAME);
        cmdStop.setBounds(10,80,150,30);
        cmdStop.setEnabled(false);
        cmdStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fireAction(ACTION_STOP);
            }
        });
        centerPanel.add(cmdStop);
        frame.add("Center", centerPanel);
    }
    
    public String getPort() {
        return edPort.getText();
    }
    
    public void showError(String msg) {
        AwtMsgBox.showMessage(frame, msg);
    }
    
    public void update(Observable source, Object arg) {
        cmdStart.setEnabled( !(model.isServerActive()) );
        cmdStop.setEnabled( (model.isServerActive()) );
        if (model.isServerActive()) {
            edPort.setText(Integer.toString(model.getPort()));
        }
        edPort.setEnabled( (!model.isServerActive()) );
    }
    
    private Button cmdStart, cmdStop;
    private Label lbPort;
    private TextField edPort;
}
