/**
 * Chat server model class
 */

package ua.vydai.chat.server.model; 

import java.io.IOException;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Observable;

import ua.vydai.chat.common.ClientServerCommands;
import ua.vydai.chat.server.model.interfaces.ServerModel;

public class ChatServerModel extends Observable implements ServerModel, Runnable {

    public ChatServerModel() {
        // Initialize the userList
        userList = new UserList();
    }
    
    /**
     * Server thread implementation
     */
    public void run() {
        Thread currentThread = Thread.currentThread();
        while(mainThread == currentThread) {
            try {
                Socket socket = serverSocket.accept(); // wait for client
                new ChatCommunication(this, socket); // create communication
            } catch (SocketException e) {
                // JavaDoc: Closes this socket. Any thread currently blocked in accept() will throw a SocketException.
                // ignore socket.accept() shutdown
            } catch (IOException e) {
                System.out.println("An exception in acceptor thread");
            }
        }
    }
    
    /**
     * Method to send a message to client
     */
    private void sendMessageToClient(Writer out, String msg) {
        try {          
            out.write(msg+"\r\n");
            out.flush();
        } catch(IOException e) {
            System.out.println("An exception with sending message to client");
        }
    }
    
    /**
     * Method to add a newUser to the userList
     */
    void addUser(ClientObject client, ChatCommunication chat) {
        
        String userName = client.getUserName();
        Socket userSocket = client.getSocket();
        Writer out = client.getOutputStream();
        if(userList.isUserExists(userName)) {
            sendMessageToClient(out, ClientServerCommands.USER_EXISTS); // user with same name already exists
            chat.close();
            return;
        } else if (userList.isSocketExists(userSocket)) {
            sendMessageToClient(out, ClientServerCommands.SOCKET_EXISTS); // client with same socket already connected
            chat.close();
            return; 
        }
        
        // Send the newUser details to other users and form userList for the newUser
        String addRFC = ClientServerCommands.ADD_USER + userName;
        StringBuffer listRFC = new StringBuffer(ClientServerCommands.USERLIST);
        
        for (ClientObject userObj : userList.getClients()) {
            sendMessageToClient(userObj.getOutputStream(), addRFC);
            listRFC.append(userObj.getUserName()).append(';');
        }
        // Add a user in to userList and listRFC
        listRFC.append(userName);
        userList.add(client, chat);
        
        // Sending complete userList to the newUser
        sendMessageToClient(out, listRFC.toString());
    }
    
    
    /**
     * Method to remove user from server
     */
    void removeUser(Socket socket) {
        ClientObject client = userList.getClientBy(socket);
        if (client != null) {
            userList.remove(client);            // remove current client from userList
            String removeRFC = ClientServerCommands.REMOVE_USER + client.getUserName();
            
            // Send a REMO RFC to all other Users
            for (ClientObject userObj : userList.getClients()) {
                sendMessageToClient(userObj.getOutputStream(), removeRFC);
            }
        }
    }
    
    /**
     * Method to send GeneralMessage
     */
    void sendGeneralMessage(Socket clientSocket, String msg) {
        String userName = userList.getClientBy(clientSocket).getUserName();
        if (userName == null) {
            return; //unknown user try to send message
        }
        // Send a general message to all users 
        String messageRFC = ClientServerCommands.GENERAL_MESSAGE + userName + ": " + msg;
        for (ClientObject userObj : userList.getClients()) {
//            if (!userObj.getUserName().equals(userName)) { // to all except sender
                sendMessageToClient(userObj.getOutputStream(), messageRFC);
//            }
        }
    }
    
    /**
     * Method to Create all Objects and start thread
     */
    public void startServer(int port) {
        // Initialize the ServerSocket
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Can't start server with port: " + port);
            return;
        }
        
        // Initialize the thread
        mainThread = new Thread(this);
        mainThread.start();
        
        setServerStatus(true);
    }
    
    /**
     * Method to Destroy the Objects
     */
    public void stopServer() {
        mainThread = null;
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch(IOException e) {
            System.out.println("Can't close serverSocket on Stop Server action");
        } finally {
            serverSocket = null;
        }
        
        // notify clients about server shutdown and close they connections
        for (ClientObject userObj : userList.getClients()) {
            sendMessageToClient(userObj.getOutputStream(), ClientServerCommands.STOP_SERVER);
        }
        userList.clear();
        setServerStatus(false);
    }
    
    private void setServerStatus(boolean status) {
        isServerActive = status;
        notifyView(null);
    }
    
    private void notifyView(Object obj) {
        setChanged();
        notifyObservers(obj);
    }
    
    public boolean isServerActive() {
        return isServerActive;
    }
    
    public int getPort() {
        return (serverSocket != null) ? serverSocket.getLocalPort() : 0;
    }
    
    public Observable observable() {
        return this;
    }
    
    public boolean isPortAvailable(int portNumber) {
        boolean portFree = true;
        ServerSocket socket = null;
        try {
            socket = new ServerSocket(portNumber);
        } catch (IOException e) {
            portFree = false;
        } finally { 
            // Clean up
            if (socket != null)
                try {
                    socket.close();
                } catch (IOException e) {
                    System.out.println("Can't close Socket after port availability check");
                } 
        }
        return portFree;
    }
    
    // Private Declarations
    private ServerSocket serverSocket;
    private UserList userList;
    private Thread mainThread;
    private boolean isServerActive = false;
}
