package ua.vydai.chat.server.model.interfaces;

public interface Closeable {

    void close();
}
