package ua.vydai.chat.server.model.interfaces;

public interface ServerAction {

    void addUser(String userName);
    void removeUser();
    void sendGeneralMessage(String msg);
}
