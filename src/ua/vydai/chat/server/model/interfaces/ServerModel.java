package ua.vydai.chat.server.model.interfaces;

import java.util.Observable;

public interface ServerModel {
    
    boolean isServerActive();
    boolean isPortAvailable(int portNumber);
    int getPort();
    void startServer(int port);
    void stopServer();
    
    Observable observable();
}
