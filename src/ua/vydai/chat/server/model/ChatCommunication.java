/**
 * Communication thread class
 */
 
package ua.vydai.chat.server.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import ua.vydai.chat.common.ClientServerCommands;
import ua.vydai.chat.server.model.interfaces.ServerAction;
import ua.vydai.chat.server.model.interfaces.Closeable;

public class ChatCommunication implements Runnable, ServerAction, Closeable {
    
    ChatCommunication(ChatServerModel chatserver, Socket clientsocket) {
        parent = chatserver;
        chatSocket = clientsocket;
        try {
            inputStream = new BufferedReader(
                    new InputStreamReader(
                            chatSocket.getInputStream(), "UTF8"));
            outputStream = new BufferedWriter(
                    new OutputStreamWriter(
                            chatSocket.getOutputStream(), "UTF8"));
        } catch(IOException e) {
            System.out.println("An error with streams initiallize");
            try {
                chatSocket.close();
            } catch (IOException ex) {
                System.out.println("Can't close chatSocket");
            }
            return;
        }
        chatThread = new Thread(this);
        chatThread.start();
    }
    
    public void run() {
        Thread currentThread = Thread.currentThread();
        while(chatThread == currentThread) {
            try {
                String msg = inputStream.readLine();
                if (msg == null) { // if input stream is empty
                    break; // end message checking
                }
                // check message by protocol
                // Message to all
                if(msg.startsWith(ClientServerCommands.USER_MESSAGE) && 
                        msg.length() > ClientServerCommands.USER_MESSAGE.length()) {
                    sendGeneralMessage(msg.substring(ClientServerCommands.USER_MESSAGE.length()));
                // Login to chat
                } else if (msg.startsWith(ClientServerCommands.HELLO) && 
                        msg.length() > ClientServerCommands.HELLO.length()) {
                    addUser(msg.substring(ClientServerCommands.HELLO.length()));
                // Quit from chat
                } else if (msg.startsWith(ClientServerCommands.QUIT)) {
                    removeUser();
                }
                
                // add new type of messages from client below
                
            } catch(Exception e) {
                System.out.println("An error in client thread");
                removeUser();
            }
        }
    }
    
    public void addUser(String userName) {
        parent.addUser(new ClientObject(chatSocket, userName, outputStream), this);
    }
    
    public void removeUser() {
        parent.removeUser(chatSocket);
    }
    
    public void sendGeneralMessage(String msg) {
        parent.sendGeneralMessage(chatSocket, msg);
    }
    
    public void close() {
        chatThread = null;
        try {
            if (chatSocket != null) {
                chatSocket.close();
            }
        } catch(IOException e) {
            System.out.println("Can't close chatSocket");
        } finally {
            chatSocket = null;
        }
    }
    
    private Thread chatThread;
    private Socket chatSocket;
    private BufferedReader inputStream;
    private BufferedWriter outputStream;
    private ChatServerModel parent;
}