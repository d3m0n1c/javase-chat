/**
 * Client class that contains information about user
 */

package ua.vydai.chat.server.model;

import java.io.Writer;
import java.net.Socket;

public class ClientObject {
    
    ClientObject(Socket socket, String userName, Writer outputStream) {
        clientSocket = socket;
        clientUserName = userName;
        clientOutputStream = outputStream;
    }
    
    public Socket getSocket() {
        return clientSocket;
    }
    
    public String getUserName() {
        return clientUserName;
    }
    
    public Writer getOutputStream() {
        return clientOutputStream;
    }
    
    private Writer clientOutputStream;
    private Socket clientSocket;
    private String clientUserName;
}